package app;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;

import java.io.IOException;

public class CarDeserializer extends StdDeserializer<Car> {

    public CarDeserializer() {
        this(null);
    }

    public CarDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Car deserialize(JsonParser p, DeserializationContext cxt) throws IOException, JsonProcessingException {
        JsonNode node = p.getCodec().readTree(p);
        int carID = node.get("carID").asInt();
        String vin = node.get("vin").asText();
        String make = node.get("make").asText();
        String model = node.get("model").asText();

        return new Car(carID, vin, make, model, new VRN(node.get("vrn").asText().toCharArray()));
    }

}