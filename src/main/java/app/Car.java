package app;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Arrays;

/**
 * Apache Commons Validator package has methods for 
 * various validations: https://commons.apache.org/proper/commons-validator/
 */

@JsonDeserialize(using = CarDeserializer.class)
public class Car {

    /**
     * A local unique identifier
     */
    private Integer carID;

    /**
     * North America VIN standard 1981
     */
    private String vin;

    /**
     * Brand of the car (e.g. Audi)
     */
    private String make;

    /**
     * A name used by manufacture to market a range
     * of similar cars (e.g. Q5)
     */
    private String model;

    private VRN vrn;
    
    /**
     * Vehicle Registration Number Sensitive and unique.
     * private String vrn;
     */

    /**
     * Photo of the car.
     * private File photo;
     */
    
    public Car() {
        super();
    }

    public Car(Integer carID, String vin, String make, String model, VRN vrn) {
        this.carID = carID;
        this.vin = vin;
        this.make = make;
        this.model = model;
        this.vrn = vrn;
    }

    public Integer getCarID() {
        return this.carID;
    }

    public String getVin() {
        return this.vin;
    }
    
    public String getMake() {
        return this.make;
    }

    public String getModel() {
        return this.model;
    }

    public String getVrn() throws Exception { return Arrays.toString(this.vrn.get()); }

    public void setCarID(Integer value) {
        this.carID = value;
    }

    public void setVin(String value) {
        this.vin = value;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String value) {
        this.model = value;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.make + " " + this.model;
    }

}
