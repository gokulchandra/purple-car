package app;

import java.util.Arrays;

final class VRN {
    final private char[] value;
    boolean consumed = false;

    VRN(char[] value) {
        this.value = value.clone();
    }

    synchronized char[] get() throws Exception {
        if (consumed) throw new Exception("Value already consumed");
        final char[] cloned = this.value.clone();
        Arrays.fill(this.value, (char) 0);
        consumed = true;
        return cloned;
    }
}
