package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@DisplayName("Security unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    //@Test
    //public void should_throwException_whenVinInvalid() { }
    //public void should_throwException_whenCarIDInvalid() { }
    //public void should_throwException_whenModelInvalid() { }
    //TODO: security!

    @Test
    public void should_setConsumed_WhenRead() throws Exception {
        String secret = "secret";
        VRN vrn = new VRN(secret.toCharArray());
        assertThat(vrn.consumed).isFalse();
        vrn.get();
        assertThat(vrn.consumed).isTrue();
    }


    @Test
    public void should_cleanupValues_WhenConsumed() {
        assertThrows(Exception.class, () -> {
            String secret = "secret";
            VRN vrn = new VRN(secret.toCharArray());
            assertThat(vrn.consumed).isFalse();
            char[] result = vrn.get();
            assertThat(result).isEqualTo(secret.toCharArray());
        });
    }

    @Test
    public void should_throwException_VrnConsumedMoreThanOnce() {
        assertThrows(Exception.class, () -> {
            String secret = "secret";
            VRN vrn = new VRN(secret.toCharArray());
            assertThat(vrn.consumed).isFalse();
            char[] result = vrn.get();
            assertThat(result).isEqualTo(secret.toCharArray());
            assertThat(vrn.consumed).isTrue();
            vrn.get();
        });
    }

}
