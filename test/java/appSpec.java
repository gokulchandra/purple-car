package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Sample unit test
     */
    @Test
    public void should_ReturnValidCarObject_whenValidInitilisation(){
        Car car = new Car(1, "3FADP4EJ9BM156937", "foo", "baz", new VRN("secret".toCharArray()));
        assertThat(car.getCarID()).isEqualTo(1);
        assertThat(car.getVin()).isEqualTo("3FADP4EJ9BM156937");
        assertThat(car.getMake()).isEqualTo("foo");
        assertThat(car.getModel()).isEqualTo("baz");
    }

    /**
     * Sample integration test
     */
    @Test
    public void should_RespondCarID_whenValidRequest() 
        throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"2\", \"vin\":\"3FADP4EJ9BM156937\", \"make\":\"foo\" ,\"model\":\"bar\",\"vrn\":\"secretStuff\" }")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("1")));
    }
}
